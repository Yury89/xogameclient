package com.kpi.XOGame.client.controller;
import com.kpi.XOGame.client.model.Model;
import com.kpi.XOGame.client.model.Point;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
public class Controller {
    private Socket socket;
    private OutputStream outputStream;
    private BufferedReader read;
    private Model model = new Model();
    private Point point;
    private Socket socket1;
    private InputStream inputStream;
    private int step;
    private ServerSocket serverSocket;

    {
        step = 0;
        setPoint();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("ok");
    }
/**
    Если наша очередь ходить то выполняется первый блок иначе второй
 */
    public void sendAddField() {
            if(isMyTurn()) {
                try {
                    System.out.println("Ваш Ход Введите координаты");
                    read = new BufferedReader(new InputStreamReader(System.in));
                    String str = read.readLine();
                    socket = new Socket("localhost", 8993);
                    outputStream = socket.getOutputStream();
                    outputStream.write(str.getBytes());
                    outputStream.flush();
                    addPoint(str, this.point);
                    System.out.println(model);
                    step++;

                }
                catch (IOException e) { e.printStackTrace();}
                finally {
                    close();
                }


            }
            else {
                try {
                    System.out.println("Ждите хода соперника");
                    int port;
                    if(point == Point.X)
                        port = 8994;
                    else port = 8992;
                    serverSocket = new ServerSocket(port);
                    socket1 = serverSocket.accept();
                    inputStream = socket1.getInputStream();
                    String str = "";
                    do {
                        str += (char) inputStream.read();
                    }
                    while (inputStream.available() > 0);
                    Point point;
                    if (this.point == Point.X)
                        point = Point.O;
                    else point = Point.X;
                    addPoint(str, point);
                    System.out.println(model);
                    step++;

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    close();
                }

            }

    }

    /**
     * Коннектимся к серверу и получаем букву за которую играем
     */
    public void setPoint(){
        try {
            socket = new Socket("localhost",8993);
            inputStream = socket.getInputStream();
            String str = "";
            do {
                str += (char) inputStream.read();
            }
            while (inputStream.available() > 0);

            if(str.equals("X")) {
                this.point = Point.X;
                System.out.println("Вы играете за Х");
            }

            if(str.equals("O")) {
                this.point = Point.O;
                System.out.println("Вы играете за O");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            close();
        }
    }

    private void close() {
        try {
            if (socket != null)
                socket.close();
            if (outputStream != null)
                outputStream.close();
            if(inputStream != null)
                inputStream.close();
            if(socket1 != null)
                socket1.close();
            if(serverSocket != null)
                serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param str1 координаты нашей точки
     * @param point тип точки Х или О
     */
    public void addPoint(String str1, Point point) {

        String[]str = str1.split(" ");
        int x = Integer.parseInt(str[0]);
        int y = Integer.parseInt(str[1]);

        this.model.field[x][y] = point;

    }

    /**
     *
     * @return если нулл то ждем пока. если поинт совпадает с нашим томы ходим
     * если не соовпадает то получаем поинт и координты соперника
     *
     */
  /*  public Point isMyTurn(){
        try {
            socket = new Socket("localhost", 8993);
            inputStream = socket.getInputStream();
            String res = "";
            do {
                res += (char) inputStream.read();
            }
            while (inputStream.available() > 0);
            if(res.equals("X"))
                return Point.X;
            if(res.equals("O"))
                return  Point.O;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            close();
        }
        return null;
    }
*/

    public boolean isMyTurn(){
        if(point == Point.X & step % 2 == 0)
            return true;
        if(point == Point.O & step % 2 != 0)
            return true;
        return false;
    }

    public Point isContinue() {
        int winX = 0;
        int winO = 0;
        for (int i = 0; i < model.field.length; i++) {
            for (int j = 0; j < model.field[i].length; j++) {
                if (model.field[i][j] == Point.O)
                    winO++;
                if (model.field[i][j] == Point.X)
                    winX++;
            }
            if (winO == 3)
                return Point.O;
            if (winX == 3)
                return Point.X;
            winO = 0;
            winX = 0;

        }
        return Point.NULL;
    }

}
