package com.kpi.XOGame.client;

import com.kpi.XOGame.client.controller.Controller;
import com.kpi.XOGame.client.model.Point;


public class Run {
    public static void main(String[] args) throws InterruptedException {
        Controller controller = new Controller();

        while (controller.isContinue() == Point.NULL) {
            controller.sendAddField();
        }
        if(controller.isContinue() == Point.X)
            System.out.println("X Won the game");
        else System.out.println("Y won the Game");

    }
}
