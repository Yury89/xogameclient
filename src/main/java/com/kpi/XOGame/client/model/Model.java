package com.kpi.XOGame.client.model;

/**
 * Created by Yuriy on 1/29/2016.
 */
public class Model {
    public Point [][] field = new Point[3][3];

    {
        for (int i = 0; i < field.length; i++)
            for (int j = 0; j < field[i].length; j++)
                field[i][j] = Point.NULL;
    }


    @Override
    public String toString() {
        String res = "";
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if(field[i][j] == Point.X)
                    res+= "X ";
                if(field[i][j] == Point.O)
                    res+= "O ";
                if(field[i][j] == Point.NULL)
                    res+= ". ";

            }
            res+="\n";

        }
        return res;
    }
}
